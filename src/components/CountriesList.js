import React from 'react';
import {
  Grid,
  Table,
  TableBody,
  TableRow,
  TableCell,
  TableContainer,
  TableHead,
  Paper,
  Typography,
} from '@material-ui/core';
import { Country } from './Country';
import './CountriesList.scss';

export default ({ countries }) => (
  <Grid container className='list-countries'>
    <Typography variant='h4'> Infected countries</Typography>
    <TableContainer component={Paper}>
      <Table aria-label='simple table'>
        <TableHead>
          <TableRow>
            <TableCell align='right'>#</TableCell>
            <TableCell>Country</TableCell>
            <TableCell align='right'>Total</TableCell>
            <TableCell align='right'>Active</TableCell>
            <TableCell align='right'>Recovered</TableCell>
            <TableCell align='right'>Death</TableCell>
            <TableCell align='right'>Date</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {countries.map((country, index) => (
            <Country {...country} key={index} index={index} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  </Grid>
);
