import React, { useEffect, useState } from 'react';
import { getAllStatistics } from '../services/WorldStatisticsService';
import { Grid, Typography } from '@material-ui/core';
import { CountriesList } from './index';
import './WorldStatistics.scss';

export default () => {
  const [statisticsCountries, setStatisticsCountries] = useState([]);
  const [totalWorldWide, setTotalWorldWide] = useState({
    totalCases: null,
    newCases: null,
    activeCases: null,
    recoveredCases: null,
    criticalCases: null,
    totalDeaths: null,
    newDeaths: null,
    totalTests: null,
  });

  const getWorlWideCases = (casesCountries) => {
    return casesCountries.reduce(
      (current, acumulator) => {
        const { cases, deaths, tests } = acumulator;
        const {
          new: newCasesCountry,
          active,
          critical,
          recovered,
          total,
        } = cases;
        const { new: newDeathsCountry, total: totalDeathsCountry } = deaths;
        const { total: newTotalTests } = tests;
        let newCasesByCountry = !newCasesCountry ? 0 : +newCasesCountry;
        let newDeathsByCountry = !newDeathsCountry ? 0 : +newDeathsCountry;
        let newTotalByTests = !newTotalTests ? 0 : +newTotalTests;

        const {
          totalCases,
          newCases,
          activeCases,
          recoveredCases,
          criticalCases,
          newDeaths,
          totalDeaths,
          totalTests,
        } = current;
        return {
          totalCases: total + totalCases,
          newCases: newCasesByCountry + newCases,
          activeCases: active + activeCases,
          recoveredCases: recovered + recoveredCases,
          criticalCases: critical + criticalCases,
          newDeaths: newDeathsByCountry + newDeaths,
          totalDeaths: totalDeathsCountry + totalDeaths,
          totalTests: newTotalByTests + totalTests,
        };
      },
      {
        ...totalWorldWide,
      }
    );
  };

  const getWorldStatistics = async () => {
    const response = await getAllStatistics();
    const totalsWorld = getWorlWideCases(response);
    setStatisticsCountries(response);
    setTotalWorldWide(totalsWorld);
  };
  useEffect(() => {
    getWorldStatistics();
  }, []);

  const {
    totalCases,
    newCases,
    activeCases,
    recoveredCases,
    criticalCases,
    newDeaths,
    totalDeaths,
  } = totalWorldWide;

  return (
    <React.Fragment>
      {totalCases && (
        <React.Fragment>
          <Grid container className='container-world-statistics-cases'>
            <Grid item xl={3} lg={3} md={6} sm={12} xs={12}>
              <Typography variant='h3'> {totalCases} </Typography>
              <Typography variant='h5'> Total Cases</Typography>
            </Grid>
            <Grid item xl={3} lg={3} md={6} sm={12} xs={12}>
              <Typography variant='h3'> +{newCases} </Typography>
              <Typography variant='h5'> New Cases</Typography>
            </Grid>
            <Grid item xl={3} lg={3} md={6} sm={12} xs={12}>
              <Typography variant='h3'> {activeCases} </Typography>
              <Typography variant='h5'> Active Cases</Typography>
            </Grid>
            <Grid item xl={3} lg={3} md={6} sm={12} xs={12}>
              <Typography variant='h3' className='critical-number'>
                {criticalCases}
              </Typography>
              <Typography variant='h5'> Critical Cases</Typography>
            </Grid>
          </Grid>

          <Grid container className='container-world-statistics-deaths'>
            <Grid item xl={6} lg={6} md={6} sm={12} xs={12}>
              <Typography variant='h3' className='new-deaths-number'>
                +{newDeaths}
              </Typography>
              <Typography variant='h5'> New Deaths</Typography>
            </Grid>
            <Grid item xl={6} lg={6} md={6} sm={12} xs={12}>
              <Typography variant='h3' className='deaths-number'>
                {totalDeaths}
              </Typography>
              <Typography variant='h5'> Total Deaths</Typography>
            </Grid>
          </Grid>

          <Grid container className='container-world-recovered'>
            <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
              <Typography variant='h3' className='recovered-number'>
                {recoveredCases}
              </Typography>
              <Typography variant='h5'> Recovered Cases</Typography>
            </Grid>
          </Grid>
          <CountriesList countries={statisticsCountries} />
        </React.Fragment>
      )}
    </React.Fragment>
  );
};
