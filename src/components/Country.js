import React from 'react';
import { TableRow, TableCell } from '@material-ui/core';
import './Country.scss';

const Country = ({ country, cases, deaths, day, index }) => {
  const { total, active, recovered } = cases;
  const { total: totalDeath } = deaths;
  return (
    <TableRow key={country} className='row-country'>
      <TableCell align='right'>{index + 1}</TableCell>
      <TableCell className='cell-country'>{country}</TableCell>
      <TableCell align='right' className='cell-total'>
        {total}
      </TableCell>
      <TableCell align='right'>{active}</TableCell>
      <TableCell align='right' className='cell-recovered'>
        {recovered}
      </TableCell>
      <TableCell align='right' className='cell-death'>
        {totalDeath}
      </TableCell>
      <TableCell align='right'>{day}</TableCell>
    </TableRow>
  );
};

export { Country };
