import React from 'react';
import { Paper, Typography } from '@material-ui/core';
import './Footer.scss';

export default () => (
  <footer className='footer'>
    <Paper elevation={3}>
      <Typography variant='h6'>Made by Fr@nklin Flores Tola</Typography>
    </Paper>
  </footer>
);
