import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import { WorldStatistics } from '../components';
import './Content.scss';

export default () => (
  <div className='container'>
    <Grid container className='title-world-wide'>
      <Typography variant='h6'>
        Made with React & Material UI & Sass & Axios
      </Typography>
    </Grid>
    <WorldStatistics />
  </div>
);
