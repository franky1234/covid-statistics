import React from 'react';
import { AppBar, Toolbar, IconButton, Typography } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';

import './Header.scss';

export default () => (
  <header className='header'>
    <AppBar position='static'>
      <Toolbar>
        <IconButton
          edge='start'
          className='icon-button'
          color='inherit'
          aria-label='menu'
        >
          <MenuIcon />
        </IconButton>
        <Typography variant='h3' className='item'>
          Coronavirus World Wide
        </Typography>
      </Toolbar>
    </AppBar>
  </header>
);
