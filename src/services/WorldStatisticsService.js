import { axiosInstance } from './Instance';

const uriWorldStatistics = 'statistics';

const getAllStatistics = async () => {
  try {
    const {
      data: { response },
    } = await axiosInstance.get(uriWorldStatistics);
    return response;
  } catch (error) {
    console.log('error WorldStatisticsSerive getAll:', error);
  }
};

export { getAllStatistics };
