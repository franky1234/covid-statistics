import axios from 'axios';
import {
  HEADER_API_HOST,
  HEADER_API_KEY,
  BASE_URL,
} from '../commons/constants';

const axiosInstance = axios.create({
  baseURL: BASE_URL,
  headers: {
    'X-RapidAPI-Host': HEADER_API_HOST,
    'X-RapidAPI-Key': HEADER_API_KEY,
  },
});

export { axiosInstance };
