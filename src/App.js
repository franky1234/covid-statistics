import React from 'react';
import { Header, Footer, Content } from './containers';
import './App.scss';

function App() {
  return (
    <div className='App'>
      <Header />
      <Content />
      <Footer />
    </div>
  );
}

export default App;
